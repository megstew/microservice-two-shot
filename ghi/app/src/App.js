import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';
import HatList from './HatList';
import ShoeList from './ShoeList';


function App() {
  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);

  async function getHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error('An Error occurred fetching the data');
    }
  }

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An Error occurred fetching the data');
    }
  }

  useEffect(() => {
    getHats();
    getShoes();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="/">
        <Route path="hats">
          <Route path="new" element={<HatForm getHats={getHats} />} />
          <Route index element={<HatList getHats={getHats} hats={hats} />} />
        </Route>
        <Route path='shoes'>
          <Route path='new' element={<ShoeForm getShoes={getShoes} />} />
          <Route index element={<ShoeList getShoes={getShoes} shoes={shoes} />} />
        </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
