import React, { useState, useEffect } from 'react';


function HatForm({getHats}) {
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picUrl, setPicUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    async function fetchLocations() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchLocations();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            fabric,
            style,
            color,
            pic_url: picUrl,
            location,
        };

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            setFabric('');
            setStyle('');
            setColor('');
            setPicUrl('');
            setLocation('');
            getHats()
        }
    }

    function handleChangeFabric(event) {
        const { value } = event.target;
        setFabric(value);
    }
    function handleChangeStyle(event) {
        const { value } = event.target;
        setStyle(value);
    }
    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }
    function handleChangePicUrl(event) {
        const { value } = event.target;
    setPicUrl(value);
    }
    function handleChangeLocation(event) {
        const { value } = event.target;
        setLocation(value);
    }
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input value={fabric} onChange={handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={style} onChange={handleChangeStyle} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                  <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="pic_url">Picture</label>
                  <input value={picUrl} onChange={handleChangePicUrl} className="form-control" type="text" id="pic_url" rows="3" name="pic_url" />
                </div>
                <div className="mb-3">
                  <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                    <option value="">Choose a location in the Wardrobe</option>
                    {locations.map(location => {
                      return (
                        <option key={location.id} value={location.href}>{location.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    );
}

export default HatForm;
