function ShoeList({getShoes, shoes}) {
  async function handleDelete(event, shoe) {
      event.preventDefault();
      const url = `http://localhost:8080/api/shoes/${shoe}`;
      const fetchConfig = {
          method: "delete",
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
          getShoes();
      }
  }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model }</td>
              <td>{ shoe.color }</td>
              <td>{ shoe.pic_href }</td>
              <td> { shoe.bin}</td>
              <td><button onClick={(event) => handleDelete(event, shoe.id)}>DELETE</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoeList;
