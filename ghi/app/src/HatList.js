function HatList({getHats, hats}) {
    async function handleDelete(event, hat) {
        event.preventDefault();
        const url = `http://localhost:8090/api/hats/${hat}`;
        const fetchConfig = {
            method: "delete",
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getHats();
        }
    }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Color</th>
            <th>Fabric</th>
            <th>Style</th>
            <th>Picture</th>
            <th>Location</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.pic_url }</td>
                <td> {hat.location}</td>
                <td><button onClick={(event) => handleDelete(event, hat.id)}>DELETE</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;
