# Wardrobify

Team:

* Person 1 - Chong Vang > Shoes Microservice
* Person 2 - Megan Stewart > Hats Microservice 

## Design
Getting Setup:

        1. Fork & clone project from repo
        2. Create and change to your own working branch
        2. create volume:
                docker volume create two-shot-pgdata
        3. create images:
                docker-compose build
        4. run docker containers:
                docker-compose up

        5.  Access Home page with this link: http://localhost:3000/
        6.  Access Wardrobify locations with this link: http://localhost:8100/api/locations/
        7.  Access Wardrobify bins with this link: http://localhost:8100/api/bins/
        8.  Access microservice shoes with this link: http://localhost:8080/api/shoes/
        9.  Access microservice hats with this link: http://localhost:8090/api/hats/

    Creating the Microservcies:

        Hats:
            Develop Backend for microservice:
                1. Register the app with the project settings
                2. Create the Hat model and location value object model
                3. Create the api view function for getting list of hats
                4. Create the api view function for creating a new hat
                5. Crate the api view function for getting hat detail and deleting a hat
                6. Write the encoeers for hat list, hat detail and location value object in views file
                7. make api url file
                8. Create the api url path for the view functions
                9. Register the app url with the project url file
                10. Write polling logic in poll.py file to get the location data from Wardrobify for the location value object (bin data for shoes)
                11. Configure api endpoints within insomnia to test functionality

            Develop Frontend for microservice:
                1. In the ghi > source directory create a HatList.js file and write a function to display list of hats
                2. In the ghi > source directory create a HatForm.js file and write a function to display a functioning form submit that handles the submit
                3. Create a single page application SPA by creating a stateful function in the App.js file to display hat list and form functionlity
                4. have App.js return react brower Router routes with the hat components
                5. Create a delete button funcationality for each hat instance and make sure page auto refreshes with updated hat list content

        Shoes:
            **Repeat all steps from hats for shoes microservice

        Final steps:
            1. add working shoes, hats, create shoe, create hat links to the nav bar

        ***Git add, commit, push to remote branch and merge with main when
        feature functionality is working!


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
